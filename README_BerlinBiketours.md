# Readme Berlin Biketours

1. Allgemeine Hinweise
2. Was leistet die App?
3. Was ist bei der Installation zu beachten?
4. Wie funktioniert die App (inkl. Screenshots)?
5. Welche Bugs sind bekannt?

## 1. Allgemeine Hinweise
Leider habe ich die App nicht zum Laufen bekommen. Vermutlich hing das aber mit irgendwelchen falschen Build-Konfigurationen zusammen. Falls es möglich ist, würde ich dem am Freitag nochmal nachgehen und diese README Datei dann aktualisieren.

## 2. Was leistet die App?
Die App ermöglicht es dem Nutzer zwischen 8 verschiedenen Fahrradrouten auszuwählen und mit Hilfe von der Google Maps API sich die gewählte Strecke auf der Karte anzeigen zu lassen. Bei Bedarf kann jederzeit über den START Button an den Anfang der Strecke gesprungen werden. Der Button ROUTEDETAILS ermöglicht es, sich in einem weiteren SeitenFragment verschiedene Sehenswürdigkeiten entlang der Route anzeigen zu lassen.

## 3. Was ist bei der Installation zu beachten?
siehe Allgemeine Hinweise

## 4. Wie funktioniert die App (inkl. Screenshots)?

![Startscreen](https://bytebucket.org/LottaRue/techmobsyst/raw/91a773c29471c61de57c295d09f9290727359b21/screenshot1.png)

![MapActivity](https://bytebucket.org/LottaRue/techmobsyst/raw/91a773c29471c61de57c295d09f9290727359b21/screenshot2.png)

![RoutenDetailsFragment](https://bytebucket.org/LottaRue/techmobsyst/raw/91a773c29471c61de57c295d09f9290727359b21/screenshot3.png)

## 5. Welche Bugs sind bekannt?
siehe Allgemeine Hinweise



























































































Welche Bugs sind bekannt?
Es fehl
















Welche Bibliotheken werden verwendet?
